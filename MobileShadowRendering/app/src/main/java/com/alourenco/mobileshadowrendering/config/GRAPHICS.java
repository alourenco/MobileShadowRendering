package com.alourenco.mobileshadowrendering.config;

public class GRAPHICS
{
    static final public int GL_CONTEXT_VERSION = 3;
    static final public int COLOR_COMPONENT_BITS = 8;
    static final public int DEPTH_BITS = 16;
    static final public int SAMPLE_BITS = 4;
    static final public int STENCIL_BITS = 0;
}
