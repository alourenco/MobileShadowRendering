package com.alourenco.mobileshadowrendering.graphics;

import android.content.Context;
import android.opengl.GLSurfaceView;

public class GLView extends GLSurfaceView
{
    private NativeRenderer _nativeRenderer;

    public GLView(Context context)
    {
        super(context);

        _nativeRenderer = new NativeRenderer();

        setRenderer(_nativeRenderer);
    }

}