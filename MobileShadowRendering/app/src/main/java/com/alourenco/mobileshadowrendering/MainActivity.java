package com.alourenco.mobileshadowrendering;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.alourenco.mobileshadowrendering.graphics.GLView;

public class MainActivity extends AppCompatActivity
{
    private GLView _glView;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        _glView = new GLView(getApplication());
        setContentView(_glView);
    }
}
